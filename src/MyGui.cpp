#include "MyGui.h"

#include "GlobalHelpersThatHaveNoHomeYet.h"

#include "sdk/gfx/imgui/imgui_impl_dx9.h"
#include "sdk/gfx/GFX3DFunction/GFXVideo3d.h"
#include "sdk/gfx/imgui/imgui_internal.h"

#include <map>
#include "engine/objects/IObject.h"
#include "engine/EntityManagerClient.h"
#include "engine/Game.h"
#include "engine/GInterface.h"

#define g_DebugMode (*reinterpret_cast<int*>(0x00EED6A7))


bool b_wnd_entitymanager = false;

struct ColumnHeader
{
    const char* label;
    float size;
};


void MyGui::MainMenuBar()
{
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			ImGui::MenuItem("Debug Mode", NULL, reinterpret_cast<bool*>(0x00EED6A7));
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("3D Engine Tools"))
		{
			ImGui::MenuItem("Entity Manager", NULL, &b_wnd_entitymanager);
			ImGui::EndMenu();
		}


		ImGui::EndMainMenuBar();
	}
}


void MyGui::EntityTool(bool *p_open)
{
	
	ImGui::Begin("EntityManager", p_open);
	
	ColumnHeader headers[] =
    {
        { "ID", 40 },
        { "Address", 70 },
        { "Type", 120 },
        { "Position", 500 }
    };

	ImGui::Columns(IM_ARRAYSIZE(headers), "TableTextureColumns", true);
	ImGui::Separator();

	float offset = 0.0f;
	for(int i = 0; i < IM_ARRAYSIZE(headers); i++)
	{
		ColumnHeader *header = &headers[i];
		ImGui::Text(header->label);
		ImGui::SetColumnOffset(-1, offset);
		offset += header->size;
		ImGui::NextColumn();
	}

	ImGui::Separator();

    static int selected = -1;
	
    for (std::map<int,CIObject*>::iterator it = g_pGfxEttManager->entities.begin(); it != g_pGfxEttManager->entities.end(); ++it)
    {
		char label[32];
    	sprintf(label, "%d", it->first);

    	if (ImGui::Selectable(label, selected == it->first, ImGuiSelectableFlags_SpanAllColumns))
			selected = it->first;
		
		ImGui::NextColumn();
		
    	ImGui::Text("%p", it->second); ImGui::NextColumn();
		ImGui::Text("%s", it->second->GetRuntimeClass()->m_lpszClassName); ImGui::NextColumn();

		ImGui::Text("(%d,%d) (%.2f, %.2f, %.2f)", 
			it->second->region.single.x,
			it->second->region.single.y,
			it->second->location.x,
			it->second->location.y,
			it->second->location.z
			); ImGui::NextColumn();

		ImGui::Separator();

    }

	ImGui::Columns(1);

	ImGui::Text("Number of Entites: %d", g_pGfxEttManager->entities.size());

	char label[32];
	sprintf(label, "Move to Entity #%d", selected);

	static bool b_checked = false;
	ImGui::Checkbox("Highlight selected", &b_checked);

	if (b_checked && selected != -1)
	{
		// ESP ON

		std::map<int,CIObject*>::iterator elem = g_pGfxEttManager->entities.find(selected);
		if (elem != g_pGfxEttManager->entities.end())
		{
			
			D3DVECTOR d2dpos, d2dpos_up, d2dpos_own;
			D3DVECTOR d3dpos = elem->second->location;
			D3DVECTOR d3dpos_up = elem->second->location;

			d3dpos_up.y += 18.0;

			if (CGFXVideo3d::get()->Project(d3dpos, d2dpos) > 0)
			{
				DrawRect(d2dpos.x-5, d2dpos.y-5, 10, 10);

				if (CGFXVideo3d::get()->Project(g_CGame->camera.character, d2dpos_own) > 0)
				{
					DXDrawLine(d2dpos_own.x, d2dpos_own.y, d2dpos.x, d2dpos.y, D3DCOLOR_ARGB(0, 255, 255, 0), 1.0);
				}


			}

			if (CGFXVideo3d::get()->Project(d3dpos_up, d2dpos_up) > 0)
			{
				DXDrawLine(d2dpos.x, d2dpos.y, d2dpos_up.x, d2dpos_up.y, D3DCOLOR_ARGB(0, 0, 255, 0), 1.0);
			}
		}
	}

	if (ImGui::Button(label))
	{
		std::map<int,CIObject*>::iterator elem = g_pGfxEttManager->entities.find(selected);
		if (elem != g_pGfxEttManager->entities.end())
		{
			g_CGInterface->m_nav.MoveTo(elem->second->region, elem->second->location);
		}
	}

	ImGui::End();
}


void MyGui::DoWork() {
	MainMenuBar();
	if (b_wnd_entitymanager) EntityTool(&b_wnd_entitymanager);
}

