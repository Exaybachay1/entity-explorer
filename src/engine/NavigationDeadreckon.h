#pragma once
#include <d3d9.h>

#include "GlobalHelpersThatHaveNoHomeYet.h"

class CNavigationDeadreckon
{
public:
	void MoveTo(uregion &region, D3DVECTOR &coord);
};

