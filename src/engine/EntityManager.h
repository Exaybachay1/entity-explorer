#pragma once

#include "sdk/gfx/GFXMainFrame/ObjChild.h"

#include <map>
#include "objects/IObject.h"

class CEntityManager : public CObjChild
{
public:
	char _gap[4];

	std::map<int, CIObject*> entities;
};
